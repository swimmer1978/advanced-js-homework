class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    set name(newName) {
        this._name = newName;
    }

    get name() {
        return this._name;
    }

    set age(newAge) {
        this._age = newAge;
    }

    get age() {
        return this._age;
    }

    set salary(newSalary) {
        this._salary = newSalary;
    }

    get salary() {
        return this._salary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = [...lang];
    }

    set lang(newLang) {
        this._lang = [...newLang];
    }

    get lang() {
        return this._lang;
    }

   set salary(newSalary) {
        this._salary = newSalary;
    }

    get salary() {
        return this._salary*3;
    }
}

const frontEndDev = new Programmer('Ivan', '21','11000', ['js', 'php', 'python']);
console.log('frontEndDeveloper', frontEndDev);
console.log('FEDeveloper salary: ', frontEndDev.salary);

const backEndDev = new Programmer('Egor', '24', '20000', ['java', 'python', 'js']);
console.log('backEndDeveloper', backEndDev);
console.log('BEDeveloper salary: ', backEndDev.salary);

const fullStackDev = new Programmer('Boris', '31', '30000', ['java', 'js', 'php', 'c#', 'c++']);
console.log('fullStackDeveloper', fullStackDev);
console.log('FSDeveloper salary: ', fullStackDev.salary);
